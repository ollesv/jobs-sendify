import React, { Component } from 'react'
import SideMenu from '@components/organisms/SideMenu'
import PageContainer from '@components/atoms/PageContainer'
import PageHeader from '@components/organisms/PageHeader'
import Content from '@components/atoms/Content'
import Compare from './pages/Compare'

import './App.sass'

export default class App extends Component {
  render() {
    return [
      <SideMenu key="sidemenu" />,
      <PageContainer key="pagecontainer">
        <PageHeader key="pageheader" />
        <Content key="content">
          <Compare />
        </Content>
      </PageContainer>
    ]
  }
}
