import React, { Component } from 'react'
import {
  minBy,
  sortBy
} from 'lodash'
import Page from '@components/atoms/Page'
import Select from '@components/atoms/Select'
import fetchData from '@components/atoms/ajax'
import CompareItem from '@components/organisms/CompareItem'
import CompareFooter from '@components/organisms/CompareFooter'
import './Compare.sass'

const lead_time_weight = 250 // Very arbitrary...
const getWeightedRecommendation = item => item.lead_time * lead_time_weight + item.cost


export default class Compare extends Component {
  state = {
    items: [],
    selected_item: null,
    sort_by: 'Most recommended',
    sort_by_key: 'recommended',
    sort_options: [
      'Most recommended',
      'Cheapest',
      'Fastest'
    ]
  }

  componentDidMount() {
    fetchData('products.json')
      .then(({ items }) => {
        items.forEach((item) => {
          const raw_name = item.product.split('_')
          item.name = `${ raw_name[0].toUpperCase() } ${ raw_name[1] }`
          item.logo = require(`../assets/img/logos/${ raw_name[0] }.svg`)
        })
        const cheapest = (minBy(items, item => item.cost)).product
        const fastest = (minBy(items, item => item.lead_time)).product
        this.setState({
          cheapest,
          fastest,
          items
        })
      })
  }

  sortItems = (items) => {
    const { sort_by_key } = this.state
    if (sort_by_key === 'recommended') {
      return sortBy(items, getWeightedRecommendation)
    }
    return sortBy(items, item => item[sort_by_key])
  }

  handleSortChange = (option) => {
    const translations = {
      'Most recommended': 'recommended',
      Cheapest: 'cost',
      Fastest: 'lead_time'
    }
    this.setState({
      sort_by: option,
      sort_by_key: translations[option]
    })
  }

  handleSelect = (selected_item) => {
    this.setState({ selected_item })
  }

  render() {
    const {
      cheapest,
      fastest,
      items,
      selected_item,
      sort_by,
      sort_options
    } = this.state

    const items_sorted = this.sortItems(items)

    return [
      <Page className="compare" key="compare">
        <Select
          label="Sort by"
          onChange={ this.handleSortChange }
          value={ sort_by }
          options={ sort_options }
        />
        {
          items_sorted.map(item => (
            <CompareItem
              item={ item }
              selected={ item === selected_item }
              cheapest={ item.product === cheapest }
              fastest={ item.product === fastest }
              onSelect={ this.handleSelect }
              key={ item.product }
            />
          ))
        }
      </Page>,
      <CompareFooter selected_item={ selected_item } key="compare-header" />
    ]
  }
}
