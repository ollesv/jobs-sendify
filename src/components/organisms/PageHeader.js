import React, { Component } from 'react'
import Header from '@components/atoms/Header'

import './PageHeader.sass'

export default () => (
  <Header flex />
)
