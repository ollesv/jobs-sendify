import React, { Component } from 'react'
import Card from '@components/atoms/Card'
import {
  Cheapest,
  Fastest
} from '@components/atoms/ItemLabels'
import {
  Text,
  Small,
  CardTitle
} from '@components/atoms/Text'
import LeadTimeBar from '@components/molecules/LeadTimeBar'
import {
  Columns,
  Column
} from '@components/atoms/columns'

import './CompareItem.sass'

export default ({
  cheapest,
  fastest,
  item,
  onSelect,
  selected
}) => {
  const {
    cost,
    currency,
    logo,
    lead_time,
    name,
  } = item
  return (
    <Card
      className={
        'compare-item' +
        (selected ? ' selected' : '')
      }
      onClick={ () => onSelect(item) }
      noPadding
    >
      <Columns>
        <Column noFlex padding className="logo">
          <img src={ logo } alt={ item.name } />
        </Column>
        <Column centerVertically paddingHorizontal>
          <CardTitle bright={ selected }>{ name }</CardTitle>
        </Column>
        <Column centerVertically>
          <Text bright={ selected }>Price</Text>
          <CardTitle bright={ selected } float>{ cost }</CardTitle>
          <Text bright={ selected } float>{ currency.toUpperCase() }</Text>
        </Column>
        <Column noFlex centerVertically paddingHorizontal>
          <LeadTimeBar
            label="Lead time: "
            unit={ lead_time > 1 ? 'Days' : 'Day' }
            value={ lead_time }
            selected={ selected }
          />
        </Column>
        { fastest && <Fastest /> }
        { cheapest && <Cheapest /> }
      </Columns>
    </Card>
  )
}
