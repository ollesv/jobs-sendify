import React, { Component } from 'react'

import './SideMenu.sass'

const logo = require('../../assets/img/logos/sendify-white-orange.svg')

export default () => (
  <div className="side-menu">
    <img src={ logo } alt="Sendify logo" className="logo" />
  </div>
)
