import React, { Component } from 'react'
import Header from '@components/atoms/Header'
import Button from '@components/atoms/Button'
import {
  Columns,
  Column
} from '@components/atoms/columns'
import {
  Text,
  Small,
  CardTitle
} from '@components/atoms/Text'

import './CompareFooter.sass'

export default ({ selected_item }) => (
  <Header fixed bottom noPadding>
    <Columns className="compare-footer">
      <Column />
      {
        selected_item
        && (
          <Column centerVertically paddingHorizontal noFlex>
            <Text>Total</Text>
            <CardTitle float>{ selected_item.cost }</CardTitle>
            <Text float>{ selected_item.currency.toUpperCase() }</Text>
          </Column>
        )
      }
      <Column centerVertically paddingHorizontal noFlex>
        <Button disabled={ !selected_item }>Next step</Button>
      </Column>
    </Columns>
  </Header>
)
