import React, { Component } from 'react'
import ProgressBar from '@components/atoms/ProgressBar'
import {
  Text
} from '@components/atoms/Text'

import './LeadTimeBar.sass'

export default ({
  label,
  selected,
  unit,
  value
}) => (
  <div className="lead-time-bar">
    <Text bright={ selected } marginBottom>{ label } <b>{ `${ value } ${ unit }` }</b></Text>
    <ProgressBar
      max={ 5 }
      value={ value }
    />
  </div>
)
