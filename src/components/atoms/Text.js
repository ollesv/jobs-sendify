import React from 'react'
import './Text.sass'

export const Title = ({
  bright,
  children,
  center,
  marginTop,
  marginBottom
}) => (
  <h1
    className={
      'title' +
      (bright ? ' text-bright' : '') +
      (center ? ' text-center' : '') +
      (marginTop ? ' text-margin-top' : '') +
      (marginBottom ? ' text-margin-bottom' : '')
    }
  >
    { children }
  </h1>
)

export const SubTitle = ({
  bright,
  children,
  center,
  float,
  marginTop,
  marginBottom
}) => (
  <h2
    className={
      'sub-title' +
      (bright ? ' text-bright' : '') +
      (center ? ' text-center' : '') +
      (float ? ' text-float' : '') +
      (marginTop ? ' text-margin-top' : '') +
      (marginBottom ? ' text-margin-bottom' : '')
    }
  >
    { children }
  </h2>
)

export const CardTitle = ({
  bright,
  children,
  center,
  float,
  marginTop,
  marginBottom
}) => (
  <h2
    className={
      'card-title' +
      (bright ? ' text-bright' : '') +
      (center ? ' text-center' : '') +
      (float ? ' text-float' : '') +
      (marginTop ? ' text-margin-top' : '') +
      (marginBottom ? ' text-margin-bottom' : '')
    }
  >
    { children }
  </h2>
)

export const ButtonText = ({
  children
}) => (
  <h2 className="text-button">
    { children }
  </h2>
)

export const Text = ({
  bright,
  bold,
  children,
  center,
  className,
  float,
  marginTop,
  marginBottom,
}) => (
  <p
    className={
      'text' +
      (bright ? ' text-bright' : '') +
      (center ? ' text-center' : '') +
      (className ? ` ${ className }` : '') +
      (float ? ' text-float' : '') +
      (marginTop ? ' text-margin-top' : '') +
      (marginBottom ? ' text-margin-bottom' : '') +
      (bold ? ' text-bold' : '')
    }
  >
    { children }
  </p>
)

export const Italic = ({
  bright,
  children,
  center,
  marginTop,
  marginBottom
}) => (
  <p
    className={
      'italic' +
      (bright ? ' text-bright' : '') +
      (center ? ' text-center' : '') +
      (marginTop ? ' text-margin-top' : '') +
      (marginBottom ? ' text-margin-bottom' : '')
    }
  >
    { children }
  </p>
)

export const Small = ({
  bright,
  children,
  center,
  marginTop,
  marginBottom
}) => (
  <p
    className={
      'small' +
      (bright ? ' text-bright' : '') +
      (center ? ' text-center' : '') +
      (marginTop ? ' text-margin-top' : '') +
      (marginBottom ? ' text-margin-bottom' : '')
    }
  >
    { children }
  </p>
)

