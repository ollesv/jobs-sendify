const domain = '/src/data/'

const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response
  }
  const error = new Error(response.statusText)
  error.response = response
  throw error
}


export default (url, options) => {
  const headers = {
    Accept: 'application/json',
    headers: {
      'Content-Type': 'application/json'
    }
  }

  return fetch(`${ domain }${ url }`, {
    headers,
    ...options
  })
    .then(checkStatus)
    .then(response => response.json())
}
