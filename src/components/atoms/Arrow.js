import React, { Component } from 'react'

import './Arrow.sass'

export default ({ className }) => (
  <div
    className={
      'arrow'
      + (className ? ` ${ className }` : '')
    }
  >
  </div>
)
