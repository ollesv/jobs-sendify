import React, { Component } from 'react'

import './Columns.sass'

export const Columns = ({ children, className }) => (
  <div
    className={
      'columns'
      + (className ? ` ${ className }` : '')
    }
  >
    { children }
  </div>
)

export const Column = ({
  centerVertically,
  children,
  className,
  noFlex,
  padding,
  paddingLeft,
  paddingRight,
  paddingHorizontal,
  paddingVertical,
  borderLeft
}) => (
  <div
    className={
      'column'
      + (className ? ` ${ className }` : '')
      + (noFlex ? ' no-flex' : ' flex')
      + (centerVertically ? ' center-vertically' : '')
      + (centerVertically ? ' center-vertically' : '')
      + (padding ? ' padding' : '')
      + (paddingLeft ? ' padding-left' : '')
      + (paddingRight ? ' padding-right' : '')
      + (paddingHorizontal ? ' padding-horizontal' : '')
      + (paddingVertical ? ' padding-vertical' : '')
      + (borderLeft ? ' border-left' : '')
    }
  >
    { children }
  </div>
)
