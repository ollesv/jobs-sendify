import React, { Component } from 'react'
import {
  ButtonText,
  Text
} from '@components/atoms/Text'
import Arrow from '@components/atoms/Arrow'

import './Select.sass'

export default class Select extends Component {
  state = {
    open: false
  }

  componentWillMount() {
    document.addEventListener('mousedown', this.handleClick, false)
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, false)
  }

  handleClick = (event) => {
    const { open } = this.state
    const { value } = this.props
    if (this.node.contains(event.target) || !open) {
      return
    }
    this.toggle(value)
  }

  toggle = (option) => {
    const { open } = this.state
    const { onChange } = this.props
    this.setState({ open: !open })
    if (option) {
      onChange(option)
    }
  }

  render() {
    const {
      label,
      options,
      value
    } = this.props

    const { open } = this.state

    return (
      <div className="select" ref={ node => this.node = node }>
        { label && <Text bold className="label">{ label }</Text> }
        <div className="select-button" onClick={ () => this.toggle() }>
          <ButtonText float>{ value }</ButtonText>
          <Arrow />
          {
            open
            && (
              <div className="options">
                {
                  options.map(option => (
                    <div className="option" key={ option } onClick={ () => this.toggle(option) }>
                      { option }
                    </div>
                  ))
                }
              </div>
            )
          }
        </div>
      </div>
    )
  }
}
