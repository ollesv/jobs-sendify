import React, { Component } from 'react'
import {
  IoIosPricetags,
  IoIosTimer
} from 'react-icons/io'

import './ItemLabels.sass'

export const Fastest = () => (
  <div className="item-label fastest">
    <IoIosTimer size="16px" color="#fff" />
  </div>
)

export const Cheapest = () => (
  <div className="item-label cheapest">
    <IoIosPricetags size="16px" color="#fff" />
  </div>
)
