import React, { Component } from 'react'

import './ProgressBar.sass'

const getStatus = (max, value) => {
  const percentage = 100 * value / max
  if (percentage <= 25) {
    return ' green'
  }
  if (percentage <= 50) {
    return ' orange'
  }
  if (percentage <= 75) {
    return ' red'
  }
  return ''
}

export default ({ max, value }) => (
  <div className="progress-bar">
    <div className="progress-bar-bg">
      <div
        className={
          'progress-bar-bar' +
          getStatus(max, value)
        }
        style={{ width: `${ 100 * value / max }%` }}
      ></div>
    </div>
  </div>
)
