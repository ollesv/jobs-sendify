import React, { Component } from 'react'

import './Card.sass'

export default ({
  children,
  className,
  noPadding,
  onClick
}) => (
  <div
    className={
      'card'
      + (className ? ` ${ className }` : '')
      + (noPadding ? ' no-padding' : '')
    }
    onClick={ onClick }
  >
    { children }
  </div>
)
