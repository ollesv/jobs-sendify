import React, { Component } from 'react'

import './PageContainer.sass'

export default ({ children }) => (
  <div className="page-container">
    { children }
  </div>
)
