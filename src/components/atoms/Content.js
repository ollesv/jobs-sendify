import React, { Component } from 'react'

import './Content.sass'

export default ({ children }) => (
  <div className="content">
    { children }
  </div>
)
