import React, { Component } from 'react'

import './Header.sass'

export default ({
  children,
  fixed,
  bottom,
  top,
  noPadding
}) => (
  <div
    className={
      'header'
      + (fixed ? ' fixed' : '')
      + (bottom ? ' bottom' : '')
      + (top ? ' top' : '')
      + (noPadding ? ' no-padding' : '')
    }
  >
    { children }
  </div>
)
