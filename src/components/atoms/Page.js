import React, { Component } from 'react'

import './Page.sass'

export default ({
  children,
  className,
  disableScroll,
  noPadding
}) => (
  <div className={
    'page'
    + (className ? ` ${ className }` : '')
    + (disableScroll ? '' : ' scroll')
    + (noPadding ? ' no-padding' : '')
  }>
    { children }
  </div>
)
