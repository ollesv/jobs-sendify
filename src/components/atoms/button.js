import React from 'react'
import './Button.sass'

export default ({
  children,
  center,
  disabled,
  right,
  onClick,
  marginTop,
  marginBottom,
  type
}) => (
  <div
    className={
      (center ? ' button-center' : '')
      + (right ? ' button-right' : '')
      + (marginTop ? ' button-margin-top' : '')
      + (marginBottom ? ' button-margin-bottom' : '')
    }
  >
    <button
      className={
        'button'
        + (center ? ' button-center' : '')
        + (disabled ? ' button-disabled' : '')
      }
      onClick={ onClick }
      type="submit"
    >
      { children }
    </button>
  </div>
)
