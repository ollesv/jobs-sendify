const path = require('path')
const webpack = require('webpack')
const HTMLWebpackPlugin = require('html-webpack-plugin')

const HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
  template: path.join(__dirname, '/src/index.html'),
  filename: 'index.html',
  inject: 'body'
})

module.exports = (env, argv) => {
  const is_production = env.mode === 'production'
  return {
    mode: is_production ? 'production' : 'development',
    entry: [
      'babel-polyfill',
      'react-hot-loader/patch',
      path.join(__dirname, '/src/index.jsx')
    ],
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loaders: ['babel-loader']
        },
        {
          test: /\.sass$/,
          loader: 'style-loader!css-loader!sass-loader'
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          loader: 'url-loader',
          options: {
            limit: 10000
          }
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        '@components': path.resolve(__dirname, 'src/components/')
      }
    },
    output: {
      filename: 'index.js',
      path: path.join(__dirname, 'dist')
    },
    plugins: is_production
      ?
      [HTMLWebpackPluginConfig]
      :
      [HTMLWebpackPluginConfig, new webpack.NamedModulesPlugin()],
    devtool: is_production ? false : 'eval-source-map'
  }
}
