## Sendify Frontend Case
This is a case to test YOUR basic understanding of frontend development and how you setup and structure YOUR projects. As this is a part of the recruitment process, it's important that you make sure to use this opportunity to show us what you want us to understand about your development. Remember that it's both about implementation and communication.

## Time
Don't overthink or overdo this. It's an opportunity to show us how you work and the main focus is to understand how you work.

## Evaluation
The result will be use as base for discussion in an upcoming interview and you will be able to tell us more in depth about you decisions.

## Case Description
The goal is to create an alternative solution to the current (compare.jpeg) in order to display and compare the products from (products.JSON) in a new screen. The user want to be able to sort the products, both based on the cheapest price and the fastest lead time. Ease of use and informative overview is in focus.

## To do list
1. Create a project. You are free to set it up as you like, but your focus should be to show us how you usually structure your projects. The goal is a one page prototype to replace (compare.jpeg). A new UI is expected.

2. Develop a solution for fetching the items from (products.JSON) and render them on your screen.

3. Develop a solution that makes it possible for the user sort the products based on either the cheapest price or the fastest lead time.

4.  

## Assets
* products.JSON - The solutions are fictional.
* Screenshot of specific part of app.sendify.se.
* Logotypes in SVG.

## License
You are not allowed to use any of the logotypes outside this project for any commercial uses without consent from the companies.

## HAPPY CODING! =D
