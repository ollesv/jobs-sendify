# Sendify frontend assignment

## Includes
- [Webpack](https://webpack.js.org)
- [React](https://reactjs.org)
- [SASS](http://sass-lang.com)
- [AirBnB eslint config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb)
- [React-hot-loader](https://github.com/gaearon/react-hot-loader)

---
## Install
Run in project root:
```
npm install
```
or
```
yarn
```

---
## Run dev server
Run in project root:
```
npm start
```
or
```
yarn start
```

Go to http://localhost:8080 in your browser.

---
## Build production assets
Run in project root:
```
npm run build
```
or
```
yarn run build
```

Production assets can be found in the dist directory in project root.
